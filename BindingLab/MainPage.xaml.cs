﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace BindingLab
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            try
            {
                untilText.Text = (DateTime.Parse("03/20/2020") - datepicker.Date).TotalDays.ToString() + " days until Animal Crossing: New Horizons comes out.";
            } catch (FormatException)
            {
                untilText.Text = "";
            }
        }

        private void datepicker_DateSelected(object sender, DateChangedEventArgs e)
        {
            untilText.Text = (DateTime.Parse("03/20/2020") - datepicker.Date).TotalDays.ToString() + " days until Animal Crossing: New Horizons comes out.";
        }
    }
}
